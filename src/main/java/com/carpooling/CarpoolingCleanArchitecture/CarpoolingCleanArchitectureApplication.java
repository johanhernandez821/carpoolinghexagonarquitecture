package com.carpooling.CarpoolingCleanArchitecture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarpoolingCleanArchitectureApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarpoolingCleanArchitectureApplication.class, args);
	}

}
